package com.samsaydali.resttemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResttemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResttemplateApplication.class, args);

        EmployeeClient employeeClient = new EmployeeClient();

        System.out.println("Getting Array of Employees");
        employeeClient.getEmployees();

        System.out.println("Getting List Wrapper of Employees");
        employeeClient.getEmployeesW();

        System.out.println("Creating Employees");
        employeeClient.createEmployees();
    }

}
