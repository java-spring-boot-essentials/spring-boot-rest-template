package com.samsaydali.resttemplate;

import com.samsaydali.resttemplate.dto.Employee;
import com.samsaydali.resttemplate.dto.EmployeeList;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;


public class EmployeeClient {


    public Employee[] getEmployees() {
        RestTemplate restTemplate = new RestTemplate();

        Employee[] employeeList =
                restTemplate
                        .getForObject("http://localhost:8080/employees/", Employee[].class);

        assert employeeList != null;
        asList(employeeList).forEach(System.out::println);

        return employeeList;
    }

    public EmployeeList getEmployeesW() {
        RestTemplate restTemplate = new RestTemplate();

        EmployeeList employeeList =
                restTemplate
                        .getForObject("http://localhost:8080/employees/v2", EmployeeList.class);

        assert employeeList != null;
        employeeList.employees.forEach(System.out::println);

        return employeeList;
    }

    public void createEmployees() {
        RestTemplate restTemplate = new RestTemplate();

        List<Employee> newEmployees = new ArrayList<>();
        newEmployees.add(new Employee(3, "Intern"));
        newEmployees.add(new Employee(4, "CEO"));

        restTemplate.postForObject(
                "http://localhost:8080/employees/v2",
                new EmployeeList(newEmployees),
                ResponseEntity.class);
    }
}
